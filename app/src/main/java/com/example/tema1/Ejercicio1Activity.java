package com.example.tema1;

import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Ejercicio1Activity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private EditText etDolares;
    private EditText etEuros;
    private  EditText etTasa;
    private Button btnConvertir;
    private double tasa = 0;
    private RadioGroup rgConversor;
    private RadioButton rbDolares;
    private  RadioButton rbEuros;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);
        etDolares = (EditText)findViewById(R.id.etDolares);
        etEuros = (EditText)findViewById(R.id.etEuros);
        etTasa = (EditText)findViewById(R.id.etTasa);
        rbDolares = (RadioButton)findViewById(R.id.rbDolares);
        rbEuros = (RadioButton)findViewById(R.id.rbEuros);
        btnConvertir = (Button)findViewById(R.id.btnConvertir);
        btnConvertir.setOnClickListener(this);
        rgConversor = (RadioGroup)findViewById(R.id.rgConversor);
        rgConversor.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        double cantidad = 0;
        if (rbDolares.isChecked()){
            try {
                cantidad = Double.parseDouble(etDolares.getText().toString());
                tasa = Double.parseDouble(etDolares.getText().toString());
                etEuros.setText(String.format("%.2f", (cantidad * tasa)));
            }catch (Exception e){
                Snackbar.make(view, "Valor no correcto", Snackbar.LENGTH_LONG).show();
            }

        }else{
            try {
                cantidad = Double.parseDouble(etEuros.getText().toString());
                tasa = Double.parseDouble(etDolares.getText().toString());
                etDolares.setText(String.format("%.2f", (cantidad / tasa)));
            }catch (Exception e){
                Snackbar.make(view, "Valor no correcto", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
        if (rbDolares.isChecked()){
            etEuros.setEnabled(false);
            etDolares.setEnabled(true);
            etDolares.setText("0,00");
            etEuros.setText("0,00");
        }else{
            etDolares.setEnabled(false);
            etEuros.setEnabled(true);
            etDolares.setText("0,00");
            etEuros.setText("0,00");
        }
    }
}
