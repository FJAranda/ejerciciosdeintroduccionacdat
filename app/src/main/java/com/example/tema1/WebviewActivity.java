package com.example.tema1;

import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebviewActivity extends AppCompatActivity {
    private WebView wvCompleto;
    private String direccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        wvCompleto = (WebView)findViewById(R.id.wvCompleto);
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null){
            direccion = bundle.getString("Direccion");
            if (direccion != null){
                    wvCompleto.loadUrl(direccion);
            }
        }
    }
}
