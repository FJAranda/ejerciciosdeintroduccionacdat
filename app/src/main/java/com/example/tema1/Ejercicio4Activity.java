package com.example.tema1;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;

public class Ejercicio4Activity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    private ImageView ivColor;
    private SeekBar sbColor;
    private SeekBar sbBrillo;
    private float brillo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(android.R.style.Theme_Material_Light_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4);

        ivColor = (ImageView)findViewById(R.id.ivColor);
        sbColor = (SeekBar)findViewById(R.id.sbColor);
        sbBrillo = (SeekBar)findViewById(R.id.sbBrillo);

        sbBrillo.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (seekBar.getId() == R.id.sbBrillo) {
            brillo = (float) i / 100;

            WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
            layoutParams.screenBrightness = brillo;
            getWindow().setAttributes(layoutParams);
        }

        if (seekBar.getId() == R.id.sbBrillo) {
            float[] hsvColor = {0, 1, 1};
            hsvColor[0] = 360f * i / 100;
            ivColor.setBackgroundColor(Color.HSVToColor(hsvColor));

        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (seekBar.getId() == R.id.sbBrillo){
            int SysBackLightValue = (int)(brillo * 255);
            android.provider.Settings.System.putInt(getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS,
                    SysBackLightValue);
        }
    }
}
