package com.example.tema1;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class Ejercicio3Activity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvCafes;
    private TextView tvTiempo;
    private Button btnMenos;
    private Button btnMas;
    private Button btnComenzar;
    private Button btnResetear;
    private CountDownTimer miContador;
    private int contadorCafes = 0;
    private int tiempo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3);

        tvCafes = (TextView)findViewById(R.id.tvCafes);
        tvTiempo = (TextView)findViewById(R.id.tvTiempo);
        btnMenos = (Button)findViewById(R.id.btnMenos);
        btnMas = (Button)findViewById(R.id.btnMas);
        btnComenzar =(Button)findViewById(R.id.btnComenzar);
        btnResetear = (Button)findViewById(R.id.btnResetear);
        tiempo = (Integer.parseInt(tvTiempo.getText().toString().substring(0,1)) * 60) +  (Integer.parseInt(tvTiempo.getText().toString().substring(3)));

        btnMenos.setOnClickListener(this);
        btnMas.setOnClickListener(this);
        btnComenzar.setOnClickListener(this);
        btnResetear.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.d("Tiempo", String.valueOf(tiempo));
        switch (view.getId()) {
            case R.id.btnMenos:
                if (tiempo > 60) {
                    tiempo -= 60;
                    Log.d("tiempo", String.valueOf(tiempo));
                    tvTiempo.setText((String.format("%02d", (tiempo / 60)) + ":" + (String.format("%02d", (tiempo % 60)))));
                    if (miContador != null) {
                        miContador.cancel();
                        miContador = null;
                    }
                }
                break;
            case R.id.btnMas:
                if (tiempo < 3540) {
                    tiempo += 60;
                    Log.d("tiempo", String.valueOf(tiempo));
                    tvTiempo.setText((String.format("%02d", (tiempo / 60)) + ":" + (String.format("%02d", (tiempo % 60)))));
                    if (miContador != null) {
                        miContador.cancel();
                        miContador = null;
                    }
                }
                break;
            case R.id.btnComenzar:
                //contadorTiempo indica los minutos
                if (tiempo > 0) {
                    if (contadorCafes < 10) {
                        miContador = new MyCountDownTimer(tiempo * 1000, 1000);
                        miContador.start();
                    }else{
                        AlertDialog.Builder popup=new AlertDialog.Builder(this);
                        popup.setTitle("Titulo del mensaje");
                        popup.setMessage("Cuerpo del mensaje");
                        popup.setPositiveButton("Ok", null);
                        popup.show();
                    }
                }
                break;
            case R.id.btnResetear:
                contadorCafes = 0;
                break;
        }
    }

    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long minutos = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
            long segundos = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - (minutos*60);
            tvTiempo.setText(String.format("%02d", minutos) + ":" + (String.format("%02d", segundos)));
        }
        @Override
        public void onFinish() {
            tvTiempo.setText("Pausa terminada!!");
            MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.ffvii);
            mp.start();
            contadorCafes++;
            tvCafes.setText(String.valueOf(contadorCafes));
        }
    }
}
