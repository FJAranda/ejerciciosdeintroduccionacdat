package com.example.tema1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Ejercicio2Activity extends AppCompatActivity {

    private Button btnIr;
    private EditText etDireccion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio2);
        btnIr = (Button)findViewById(R.id.btnIr);
        etDireccion = (EditText)findViewById(R.id.etDireccion);

        btnIr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Ejercicio2Activity.this, WebviewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("Direccion", etDireccion.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
